extends Node

signal score
signal score2
signal iwon

var scorePlayerOne = 0
var scorePlayerTwo = 0                                # 0  1   in out b 
var matrix = [#  in    out    blocco                   ______
              [false, false], #false   #Viola      # 0|F | F |  T  F  T
              [false, false], #false   #Verde      # 1|F | F |  F  F  F
              [false, false], #false   #Azzurro    # 2|F | F |  ...   F
              [false, false], #false   #rosa       # 3|F | F |  ...   F     
              [false, false]  #false   #rosa2      # 4|F | F |  ...   F
                                        ]  #  -------
var punteggio = [1, 5, 3, 2, 4]
var turno = 0 
            # lock  turno
var lock = [[false, turno], 
            [false, turno ],
            [false, turno],
            [false, turno ],
            [false, turno]
            ]

func _player():
	turno += 1
	for i in range(lock.size()):
		if lock[i][0] && lock[i][1] < turno -1:
			lock[i][0] = false
	if $Nano.focused_player == 0: 
		$Nano.focused_player = 1
		$Nano/Verde.hide()
		$Nano/Blue.show()
	elif $Nano.focused_player ==1:
		$Nano.focused_player = 0
		$Nano/Verde.show()
		$Nano/Blue.hide() 

func _process(delta):
	if scorePlayerOne >= 10 || scorePlayerTwo >= 10:
		get_tree().paused = true
		if scorePlayerOne > scorePlayerTwo: #p1 > p2 true 
			emit_signal("iwon", "Giocatore uno hai vinto")
		else: emit_signal("iwon", "Giocatore due hai vinto") #p2 > p1

func _on_Nano_tessera_launched(tessera):
	tessera.connect("stopped", self, "_on_Tessera_stopped")

func _on_Tessera_stopped(tessera):
	for i in range(matrix.size()):
		if matrix[i][0]  && !matrix[i][1]: # cosa abbiamo preso 
			if lock[i][0]:
					continue
			else: lock[i][0] = true 
			lock[i][1] = turno
			if $Nano.focused_player == 0 : # && sono stato bloccato?
				scorePlayerOne += punteggio[i]
				emit_signal("score", scorePlayerOne)
			elif $Nano.focused_player == 1:
				scorePlayerTwo += punteggio[i]
				emit_signal("score2", scorePlayerTwo)
		tessera.queue_free()
		matrix[i][0] = false 
		matrix[i][1] = false
	_player()

func _on_Viola_body_entered(body):
	matrix[0][0] = true
 
func _on_Viola_body_exited(body):
	if !body.is_queued_for_deletion ( ):
		matrix[0][1] = true

func _on_Verde_body_entered(body):
	matrix[1][0] = true

func _on_Verde_body_exited(body):
	if !body.is_queued_for_deletion ( ):
		matrix[1][1] = true

func _on_Azzurro_body_entered(body):
	matrix[2][0] = true 

func _on_Azzurro_body_exited(body):
	if !body.is_queued_for_deletion ( ):
		matrix[2][1] = true 

func _on_Rosa_body_entered(body):
	matrix[3][0] = true

func _on_Rosa_body_exited(body):
	if !body.is_queued_for_deletion ( ):
		matrix[3][1] = true

func _on_Rosa2_body_entered(body):
	matrix[4][0] = true

func _on_Rosa2_body_exited(body):
	if !body.is_queued_for_deletion ( ):
		matrix[4][1] = true 