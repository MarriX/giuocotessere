extends KinematicBody2D

var tessere_scena = load("res://Entita/Tessere/Tessere.tscn")

export var tessera_counter = 0
export var focused_player = 0
#var nano_speed = [0, 450, 550, 650, 750, 850, 950, 1050]
var NANO_SPEED = 750
var speed = Vector2()
var motion = Vector2()
var left = Vector2(-1, 0)
var right = Vector2(1, 0)
var dir = left

signal tessera_launched(tessera)

func _process(delta):
	if $CoolDown.time_left == 0 and Input.is_action_pressed("ui_accept"):
		$CoolDown.start()
		NANO_SPEED = 0
		speed.y = speed.y + 98
	if Input.is_action_just_released("ui_accept"):
		launch_tessere(speed.y)
		NANO_SPEED = 750
		speed.y = 0

func _physics_process(delta):
	motion.x = dir.x * NANO_SPEED
	motion = move_and_slide(motion)
	if is_on_wall():
		if dir == left:
			dir = right
		elif dir == right:
			dir = left

func launch_tessere(var vel):
	var tessere_node = tessere_scena.instance()
	emit_signal("tessera_launched", tessere_node)
	tessere_node.position = self.position + $RayCast2D.cast_to.normalized()*32
	tessere_node.apply_impulse(Vector2(), $RayCast2D.cast_to.normalized()*vel)
	print("Dio Porco! ", tessere_node.id)
	get_node("/root/Node").add_child(tessere_node)