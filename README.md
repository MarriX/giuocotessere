# GiuocoTessere

GioucoTessere is a game made with the Godot Engine.  The point of this
project is to create a fun game, without forgetting about competitiveness.

GiuocoTessere is a two player game, player vs player, whose objective is
to hit targes to accumulate more points than the adversary.

![GiuocoTessere example](screen.png)

### Features

 - multi-platform (OpenBSD, Linux, macOS, Windows)
 - OP from the beta (that has yet to be released)
