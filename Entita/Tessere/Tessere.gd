extends RigidBody2D

export(int) var id = 0

export(int) var speed = 0
const THRESHOLD = -0.03
signal stopped(tessera)
var emitted = false

	
func _physics_process(delta):
	# velocita sull'asse Y. Attenzione: è negativa! (perchè
	# l'asse Y va dall'alto verso il basso, la tessera andando
	# verso l'alto si muove in verso opposto.)
	var vy = self.linear_velocity[1] * delta 
	if vy != 0 && vy >= THRESHOLD && !emitted: 
		emitted = true
		print("emitted stopped!")
		emit_signal("stopped", self)